#ifndef UTIL_H
#define UTIL_H

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stddef.h>
#include <errno.h>
#include <time.h>
#define SUCCESS_FLAG 0
#define INFO_FLAG 1
#define ERROR_FLAG 2

#define WINNER 1
#define LOOSER -1

struct stack {
    char *word;
    struct stack *next;
};


/* Logging */
void log_msg(const char *msg, int flag);
void error_exit(const char *msg);
void error_continue(const char *msg);

/* Displaying */
void display_matrix(const char **matrix, size_t size);
void display_wordlist(char **wordlist, int size);

/* Utilities */
int get_random_number(int min, int max);
char *get_random_word(char **wordlist, size_t wordlist_size);
char get_random_character();

/* stack data structure */
struct stack *create_stack(const char *word); 
void push(struct stack **st, const char *word);
char *pop(struct stack **st);
int is_empty(struct stack *st);
void traverse(struct stack *st);


#endif
