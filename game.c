#include "game.h"

struct player *create_player(const char *username, size_t score) {
    struct player *pl = malloc(sizeof(struct player));
    strcpy(pl->username, username);
    pl->score = score;
    return pl;
}

void add_score(struct player *pl, size_t sc) {
    if ( pl != NULL ) {
        pl->score += sc;
    }
}

void display_player(struct player *player) {
    if ( player == NULL )
        return;
    fprintf(stdout, "[?] Player: %s, Score: %d\n",
                player->username,
                player->score
            );
}


char **read_wordlist (const char *filename, int n) {
	int wordlist_size = n;
    char **wordlist = malloc(sizeof(char*) * wordlist_size);
	
	// open wordlist file 
	FILE *file = fopen(filename, "r");
	if ( file == NULL ) {
		perror("Error: wordlist not found !!");
		exit(EXIT_FAILURE);
	}
	
	int counter = 0;
	char *line = malloc(sizeof(char)*5);

	// read wordlist line by line
    puts("[*] Reading wordlist ...");
	while ( fscanf(file, "%s", line) != EOF ) {
        
        if ( counter >= wordlist_size ) {
            // reallocate memory, add 10 cells
            wordlist = realloc(wordlist, sizeof(char*) * (wordlist_size + 10));
            wordlist_size += 10;
        }

        *(wordlist + counter) = malloc(sizeof(char) * 5);
		strcpy(*(wordlist + counter), line);
		counter++;
	}

	fclose(file);
	free(line);
	return wordlist;
}

char **generate_matrix (char **wordlist, size_t wordlist_size) {
    int matrix_size = 4;

    // create and allocate memory for matrix
    puts("[*] Allocating memory for matrix ...");
    char **matrix = malloc(sizeof(char*) * matrix_size);
    for (int i = 0; i < 4; i++) {
        *(matrix + i) = malloc(sizeof(char) * (matrix_size + 1));
        memset((void*) *(matrix +i), '.', matrix_size);
    }

    // create stack of random word from wordlist
    puts("[*] Creating random stack of words ...");
    struct stack *st = create_stack(get_random_word(wordlist, wordlist_size));
    int i = 0;
    while ( i++ < wordlist_size / 2 )
        push(&st, get_random_word(wordlist, wordlist_size));

    // construct matrix
    puts("[*] Constructing matrix ...");
    while ( ! is_empty(st) ) {
        char *word = pop(&st);
        place_word(matrix, word, strlen(word));
    }

    // fill empty boxes with random characters
    puts("[*] Filling empty cells ...");
    for ( int i = 0; i < matrix_size; i++ ) {
        for (int j = 0; j < matrix_size; j++) {
            if ( matrix[i][j] == '.' ) 
                matrix[i][j] = get_random_character();
        }
    }

	return matrix;
}

int is_valid_word(char **wordlist, int wordlist_size, char* word) {
    fprintf(stdout, "[*] Comparing word (%s) against wordlist ...\n", word);
    int is_valid = 0;
    for (int i = 0; i < wordlist_size; i++) {
        is_valid = 1;

        for (int c = 0; c < strlen(wordlist[i]); c++) {
            int ch1 = toupper(wordlist[i][c]);
            int ch2 = toupper(word[c]);
            if ( ch1 != ch2 )
                is_valid = 0; 
        }
        if ( is_valid )
            return is_valid;

    }
    return is_valid;
}


void init_game_data(struct game_data *game_data,
                    char **matrix,
                    struct player *player,
                    char **wordlist,
                    int wordlist_size
                    ) {

    game_data->matrix = matrix;
    game_data->matrix_size = 4;
    game_data->player = player;
    game_data->wordlist = wordlist;
    game_data->wordlist_size = wordlist_size;
    game_data->word_size = 0;
}

int place_word(char **matrix, char *word, size_t word_length) {
    int matrix_size = 4;
    int x = get_random_number(0, matrix_size-1);
    int y = get_random_number(0, matrix_size-1);

    if ( word_length == 4 ) {
        // move to edge cells
        if ( x == 1 )
            y = 3;
        if ( x == 2 )
            y = 0;
    }

    int i = x, j = y;
    int counter = 0;
    while (counter++ < 9) {
        int orientation = get_random_number(1, 8);
        if ( is_path_clear(matrix, matrix_size, word, x, y, orientation) > 0 ) {
            put_word(matrix, word, x, y, orientation);
            return 1;
        }
    }
    return -1;
}

  
int is_path_clear(char **matrix, size_t matrix_size, char *word, size_t x, size_t y, size_t orientation) {
    int overflow_x, overflow_y;

    switch ( orientation ) {
        case H_L_R:
            overflow_y = y + strlen(word) - 1;
            if ( overflow_y <  matrix_size) {
                for (int i = 0; i < strlen(word); i++) {
                    if ( matrix[x][y+i] != '.' && matrix[x][y+i] != word[i] )
                        return 0;
                }
                return 1;
            }
            return 0;
        case H_R_L: 
            overflow_y = y - strlen(word) + 1;
            if ( overflow_y >= 0 ) {
                for (int i = 0; i < strlen(word); i++)  {
                    if ( matrix[x][y-i] != '.' && matrix[x][y-i] != word[i] )
                        return 0;
                }
                return 1;
            }
            return 0;
        case V_T_B:
            overflow_x = x + strlen(word) - 1;
            if ( overflow_x <  matrix_size ){ 
                for (int i = 0; i < strlen(word); i++) {
                    if ( matrix[x+i][y] != '.' && matrix[x+i][y] != word[i] )
                        return 0;
                }
                return 1;
            }
            return 0;
        case V_B_T:
            overflow_x = x - strlen(word) + 1;
            if ( overflow_x >= 0 ) {
                for (int i = 0; i < strlen(word); i++) {
                    if ( matrix[x-i][y] != '.' && matrix[x-i][y] != word[i] )
                        return 0;
                }
                return 1;
            }
            return 0;
            
        case D_TL_BR:
            overflow_x = x + strlen(word) - 1;
            overflow_y = y + strlen(word) - 1;
            if ( (overflow_x < matrix_size ) && ( overflow_y < matrix_size )) {
                for (int i = 0; i < strlen(word); i++) {
                    if ( matrix[x+i][y+i] != '.' && matrix[x+i][y+i] != word[i] )
                    return 0;
                }
                return 1;
            }
            return 0;
        case D_BR_TL:
            overflow_x = x - strlen(word) + 1;
            overflow_y = y - strlen(word) + 1;
            if ( ( overflow_x >= 0 ) && ( overflow_y >= 0 ) ) {
                for (int i = 0; i < strlen(word); i++) {
                    if ( matrix[x-i][y-i] != '.' && matrix[x-i][y-i] != word[i] )
                        return 0;
                }
                return 1;
            }
            return 0;

        case D_BL_TR:
            overflow_x = x - strlen(word) + 1;
            overflow_y = y + strlen(word) - 1;
            if ( ( overflow_x >= 0 ) && ( overflow_y < matrix_size ) ) {
                for (int i = 0; i < strlen(word); i++) {
                        if ( matrix[x-i][y+i] != '.' && matrix[x-i][y+i] != word[i] )
                        return 0;
                }
                return 1;
            }
            return 0;

        case D_TR_BL:
            overflow_x = x + strlen(word) - 1;
            overflow_y = y - strlen(word) + 1;
            if ( ( overflow_x < matrix_size ) && ( overflow_y >= 0 ) ) {
                for (int i = 0; i < strlen(word); i++) {
                    if ( matrix[x+i][y-i] != '.' && matrix[x+i][y-i] != word[i] )
                        return 0;
                }
                return 1;
            }
            return 0;
        default: return 0;
    }
}


void put_word(char **matrix, char *word, size_t x, size_t y, size_t orientation) {
    switch ( orientation ) {
        case H_L_R:
            for (int i = 0; i < strlen(word); i++)
                matrix[x][y+i] = word[i];
            break;
        case H_R_L: 
            for (int i = 0; i < strlen(word); i++)
                matrix[x][y-i] = word[i];
            break;
        case V_T_B:
            for (int i = 0; i < strlen(word); i++)
                matrix[x+i][y] = word[i];
            break;
        case V_B_T:
            for (int i = 0; i < strlen(word); i++)
                matrix[x-i][y] = word[i];
            break;
         case D_TL_BR:
            for (int i = 0; i < strlen(word); i++)
                matrix[x+i][y+i] = word[i];
            break;
        case D_BR_TL:
            for (int i =0; i < strlen(word); i++)
                matrix[x-i][y-i] = word[i];
            break;
        case D_BL_TR:
            for (int i =0; i < strlen(word); i++)
                matrix[x-i][y+i] = word[i];
            break;
        case D_TR_BL:
            for (int i = 0; i < strlen(word); i++)
                matrix[x+i][y-i] = word[i];
            break;
    }
}


