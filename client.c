#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <getopt.h>
#include <gtk/gtk.h>

#include "game.h"
#include "gui.h"
#include "network.h"

struct player *player;
int sock;

void usage (const char *filename) {
    fprintf(stdout, "usage: %s -s <server> -p <port> -u <username>\n", filename);
    exit(EXIT_FAILURE);
}

void parse_args(int argc, char **argv, char *server, int *port, char *username) {

    int opt;
    int flags = 0;
    while ( (opt=getopt(argc, argv, "s:p:u:")) != -1 ) {
        switch ( opt ) {
            case 's':
                strcpy(server, optarg);
                flags++;
                break;
            case 'p':
                *port = atoi(optarg);
                flags++;
                break;
            case 'u':
                strcpy(username, optarg);
                flags++;
                break;
            default:
                usage(argv[0]);
        }
    }
    if ( flags != 3 )
        usage(argv[0]);

}

void alarm_handler(int d) {
    puts("alarma handler is called");
    send_player_score(sock, player);
    int result = recv_game_result(sock);
   
    // display dialog msg
    if ( result == WINNER ) {
        display_game_result("Congratulations", "You are the WINNER :)");
        log_msg("YOU ARE THE WINNER", SUCCESS_FLAG);
    } else {
        display_game_result("Sorry", "You have lost");
        log_msg("YOU ARE THE LOOSER", SUCCESS_FLAG);
    }

    close(sock);
    exit(0);
}

int main (int argc, char **argv) {

    signal(SIGALRM, alarm_handler);
    
    // init variable
    char *server = malloc(sizeof(char)*32);
    char *username = malloc(sizeof(char)*64);
    int port;
    player = malloc(sizeof(struct player));
    struct game_data *game_data = malloc(sizeof(struct game_data));
    int wordlist_size;
    char **matrix;
    char **wordlist;
    GtkBuilder *builder;
    GtkWidget *main_window;
    GtkWidget *play_button;
    GtkWidget *exit_button;

    parse_args(argc, argv, server, &port, username);
    fprintf(stdout, "Server: %s, Port: %d, Username: %s\n", 
            server, port, username);

    sock = create_client_socket(server, port);
    if ( sock < 0 ) {
        perror("[-] Failed to connect to server, no sock is created");
        exit(EXIT_FAILURE);
    }

    // connect to server
    struct sockaddr_in server_addr;
    server_addr.sin_family = AF_INET;
    server_addr.sin_port = htons(port);
    inet_aton(server, &(server_addr.sin_addr));

    if ( connect(sock, (struct sockaddr*) &server_addr, (socklen_t) sizeof(server_addr)) < 0 ) {
        perror("[-] Error connecting to server ...\n");
        exit(EXIT_FAILURE);
    } else {
        fprintf(stdout, "[+] Connected to server %s on port %d\n",
                    server, port);

        // send playe info
        strcpy(player->username, username);
        player->score = 0;
        send_player_info(sock, player);

        // recv game data 
        wordlist = recv_wordlist(sock,(size_t*) &wordlist_size);
        matrix = recv_matrix(sock);

        display_matrix((const char**)matrix, 4);
        display_wordlist(wordlist, wordlist_size);

        // init game data
        init_game_data(game_data, matrix, player, wordlist, wordlist_size);
        game_data->socket = sock;

        // init, build and display main UI
        gtk_init (&argc, &argv);
        init_home_ui(builder, main_window, play_button, exit_button, game_data);

    }


    close(sock);
    free(matrix);
    free(wordlist);
    free(player);
    free(server);
    free(username);
    return 0;
}
