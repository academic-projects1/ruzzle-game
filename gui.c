#include "gui.h"

void init_home_ui(
                GtkBuilder *builder,
                GtkWidget *main_window,
                GtkWidget *play_button,
                GtkWidget *exit_button,
                struct game_data *game_data
                ) {

        builder = gtk_builder_new_from_file ("gtk_xml/main_window.glade");
        main_window = GTK_WIDGET (gtk_builder_get_object(builder, "main_window"));
        play_button = GTK_WIDGET (gtk_builder_get_object(builder, "play_button"));
        exit_button = GTK_WIDGET (gtk_builder_get_object(builder, "exit_button"));


        // connecting signals
        gtk_builder_connect_signals (builder, NULL);
        g_signal_connect (main_window, "destroy", G_CALLBACK(gtk_main_quit), NULL);
        g_signal_connect (exit_button, "clicked", G_CALLBACK(gtk_main_quit), NULL);
        g_signal_connect (play_button, "clicked", G_CALLBACK(start_game), game_data);


        gtk_widget_show(main_window);
        gtk_main();
}

void start_game (GtkButton *button, struct game_data *game_data) {
    fprintf(stdout, "[+] starting game ...\n");

    send_notification(game_data->socket, START_GAME);

    // init data
    char *user_label_str = malloc(64);
    char *score_label_str = malloc(64);
    strcpy(game_data->word, "....");
    sprintf(user_label_str, "User: %s", game_data->player->username);
    sprintf(score_label_str, "Score: %d", game_data->player->score);

    // build UI
    GtkBuilder *builder = gtk_builder_new_from_file ("gtk_xml/matrix_window.glade");
    GtkWidget *window = GTK_WIDGET (gtk_builder_get_object(builder, "matrix_window"));
    GtkLabel *user_label = GTK_LABEL (gtk_builder_get_object(builder, "user_label"));
    GtkLabel *time_label= GTK_LABEL (gtk_builder_get_object(builder, "time_label"));
    GtkLabel *score_label = GTK_LABEL (gtk_builder_get_object(builder, "score_label"));

    // binding data
    gtk_label_set_text(user_label, user_label_str);
    gtk_label_set_text(score_label, score_label_str);

    // connect signal
    g_signal_connect (window, "destroy", G_CALLBACK(gtk_main_quit), NULL);

    // plot mtrix
    plot_matrix(builder, game_data);

    // display window
    recv_notification(game_data->socket);
    alarm(10);
    gtk_widget_show(window);
    gtk_main();
}

void plot_matrix (GtkBuilder *builder, struct game_data *game_data ) {

    char cell_name[8];
    char ch[2];
    char **matrix = game_data->matrix;
    int matrix_size = 4;
    GtkButton *cell_button;
    for (int i = 0; i < matrix_size; i++) {
        for (int j = 0; j < matrix_size; j++) {
            sprintf(cell_name, "cell_%d%d", i, j);
            ch[0] = toupper(matrix[i][j]);
            ch[1] = '\0';
            cell_button = GTK_BUTTON (gtk_builder_get_object(builder, cell_name));
            gtk_button_set_label(cell_button, ch);
            g_signal_connect (cell_button, "clicked", G_CALLBACK(mark_cell), game_data);
        }
    }

}

void mark_cell (GtkButton *button, struct game_data *game_data) {
    char *cell_name = (char*)gtk_button_get_label(button);

    // add clicked characted to word
    game_data->word[game_data->word_size++] = cell_name[0];

    if ( is_valid_word(game_data->wordlist, game_data->wordlist_size, game_data->word) ) {
        add_score(game_data->player, 1);
        
        fprintf(stdout, "[+] Found Valid Word, Adding Score ...");
        //reset
        strcpy(game_data->word, "....");
        game_data->word_size = 0;
    }
    
    if ( game_data->word_size == 4 ) {
        strcpy(game_data->word, "....");
        game_data->word_size = 0;
    }

    
    fprintf(stdout, "Cosntructed word: %s\n", game_data->word);
}


void display_game_result(const char *msg, const char *description) {
        GtkBuilder *builder = gtk_builder_new_from_file ("gtk_xml/msg_window.glade");
        GtkWidget *main_window = GTK_WIDGET (gtk_builder_get_object(builder, "msg_window"));
        GtkWidget *msg_title= GTK_WIDGET (gtk_builder_get_object(builder, "msg_title"));
        GtkWidget *msg_description= GTK_WIDGET (gtk_builder_get_object(builder, "msg_description"));

        // connecting signals
        gtk_builder_connect_signals (builder, NULL);
        gtk_label_set_text(GTK_LABEL(msg_title), msg);
        gtk_label_set_text(GTK_LABEL(msg_description), description);
        
        gtk_widget_show(main_window);
        gtk_main();
}
