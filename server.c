#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <getopt.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <pthread.h>

#include "util.h"
#include "game.h"
#include "network.h"
#define MAX_THREADS 20

extern char *optarg;
extern int optind, opterr, optopt;
pthread_t threads[MAX_THREADS];
int sockets[MAX_THREADS];
struct request_data *requests[MAX_THREADS];
int counter = 0;
pthread_mutex_t lock = PTHREAD_MUTEX_INITIALIZER;
pthread_cond_t cond = PTHREAD_COND_INITIALIZER;

void usage (const char *filename) {
    fprintf(stdout, "usage: %s -w <wordlist> -n <lines> -p <port>\n", filename);
    exit(EXIT_FAILURE);
}
void parse_args(int argc, char **argv, char *filename, int *n, int *port) {
    int opt;
    int flags = 0;
    while ( (opt = getopt(argc, argv, "w:n:p:")) != -1 ) {
        switch (opt) {
            case 'w':
                strcpy(filename, optarg);
                flags++;
                break;
            case 'n':
                *n = atoi(optarg);
                flags++;
                break;
            case 'p':
                *port = atoi(optarg);
                flags++;
                break;
            default:
                usage(argv[0]);
        }
    }
    if ( flags != 3 )
        usage(argv[0]);
}

int main (int argc, char **argv) {

    // parse args
    int n, port;
    char *filename = malloc(sizeof(char)*64);
    parse_args(argc, argv, filename, &n, &port);

    struct sockaddr_in client1_addr;
    struct sockaddr_in client2_addr;


    int socket = create_server_socket(port);
    if ( socket < 0 ) {
        perror("[-] Failed to start server, no socket is created\n");
        exit(EXIT_FAILURE);
    }

    if ( listen(socket, 20) != 0 ) {
        perror("[-] Error listening for connection!!!\n");
        close(socket);
        exit(EXIT_FAILURE);
    } else {
        fprintf(stdout, "[+] Listening ...\n");
    }
   
    int length1 = sizeof(client1_addr);
    int length2 = sizeof(client2_addr);
    while ( 1 ) {
        int player1_sock;
        int player2_sock;

        char **wordlist = read_wordlist(filename, n);
        char **matrix = generate_matrix(wordlist, n);

        struct request_data request_data;
        request_data.wordlist_size = n;
        request_data.matrix_size = 4;
        request_data.wordlist = wordlist;
        request_data.matrix = matrix;

        player1_sock = accept(socket, (struct sockaddr*) &client1_addr, &length1);
        if ( player1_sock < 0 ) {
            fprintf(stdout, "[-] Error accepting connection !!\n");
            fprintf(stderr, "[-] Error: %s\n", strerror(errno));
            continue;
        } else
            log_msg("Accepted the first connection\n", INFO_FLAG);

        player2_sock = accept(socket, (struct sockaddr*) &client2_addr, &length2);
        if ( player2_sock < 0 ) {
            fprintf(stdout, "[-] Error accepting connection !!\n");
            fprintf(stderr, "[-] Error: %s\n", strerror(errno));
        } else
            log_msg("Accepted the second connection\n", INFO_FLAG);

        request_data.sock1 = player1_sock;
        request_data.sock2 = player2_sock;

        if ( pthread_create(&threads[counter++], NULL, handle_request, (void*) &request_data) < 0 ) {
            fprintf(stdout, "[-] Failed to spawn thread\n");
            fprintf(stderr, "[-] Error: %s\n", strerror(errno));
        }
        
    }

    free(filename);
    return 0;
}
