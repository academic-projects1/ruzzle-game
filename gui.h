#ifndef GUI_H
#define GUI_H

#include <stdio.h>
#include <gtk/gtk.h>
#include <ctype.h>

#include "util.h"
#include "game.h"
#include "network.h"


/* build and displaye the Home UI */
void init_home_ui(
                GtkBuilder *builder,
                GtkWidget *main_window,
                GtkWidget *play_button,
                GtkWidget *exit_button,
                struct game_data *game_data
                );

/* build the game UI and start the game */
void start_game (GtkButton *button, struct game_data *user_data);

/* plot the matrix in *game_data in the UI using *builder */
void plot_matrix (GtkBuilder *builder, struct game_data *game_data);

/* keep track of selected characters and check valid words */
void mark_cell (GtkButton *button, struct game_data *game_data);

/* build and display End game UI with *msg and a *description */
void display_game_result(const char *msg, const char *description);

#endif
