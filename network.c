#include "network.h"


int create_server_socket(int port){

	// create socket
	int sock;
	if ( (sock = socket(AF_INET, SOCK_STREAM, 0)) < 0 ) {
		perror("[-] Rrror creating socket"); 
        return -1;
	}

	// create sockeaddr
	struct sockaddr_in addr;
	memset(&addr, 0, sizeof(struct sockaddr));
	addr.sin_family = AF_INET;
	addr.sin_port = htons(port);
	inet_aton("0.0.0.0", &(addr.sin_addr));
	
	// bind addr
	if ( (bind(sock, (struct sockaddr*)&addr, sizeof(struct sockaddr_in)) < 0 )) {
		perror("error binding");
        return -1;
	}
	fprintf(stdout, "[*] Server binding socket...");

	// get addresse of bind socket
	struct sockaddr_in bound_addr;
	socklen_t *lenght = malloc(sizeof(socklen_t));
	*lenght = sizeof(struct sockaddr_in);
	if ( getsockname(sock, (struct sockaddr*) &bound_addr, lenght) != 0 ) {
		perror("[-] Error: getting sockname error");
        return -1;
	}
	fprintf(stdout, "addr: %s\n", inet_ntoa(bound_addr.sin_addr));

    return sock;
}

int create_client_socket (const char *server, int port){

	// create socket
	int sock;
	if ( (sock = socket(AF_INET, SOCK_STREAM, 0) ) < 0 ) {
		perror("[-] Error creating socket");
		return -1;
	}
	
	// create addr struct
	struct sockaddr_in *addr = malloc(sizeof(struct sockaddr_in));
	addr->sin_family = AF_INET;
	addr->sin_port = htons(port);
	if ( inet_aton(server, &(addr->sin_addr)) == 0 ) {
		perror("[-] Error creating addr");
		return -1;
	}

    return sock;
}

void *handle_request(void *request_data) {

    log_msg("handling request ...\n", INFO_FLAG);

    struct request_data *data = (void*) request_data;
    int sock1 = data->sock1;
    int sock2 = data->sock2;
    struct player pl1;
    struct player pl2;

    // recv players info
    log_msg("recieving player info ..", INFO_FLAG);
    recv_player_info(sock1, &pl1);
    recv_player_info(sock2, &pl2);

    // send wordlist to players
    log_msg("sending wordlist to players ...", INFO_FLAG);
    send_wordlist(sock1, data->wordlist, data->wordlist_size);
    send_wordlist(sock2, data->wordlist, data->wordlist_size);

    // send matrix to players
    log_msg("sending matrix to players ...", INFO_FLAG);
    send_matrix(sock1, data->matrix);
    send_matrix(sock2, data->matrix);

    // start the game for both player at the same time
    log_msg("waiting for players disponibility ...", INFO_FLAG);
    recv_notification(sock1);
    recv_notification(sock2);

    log_msg("sending notificaton to start game ...", INFO_FLAG);
    send_notification(sock1, START_GAME);
    send_notification(sock2, START_GAME);

    
    // wait for signal
    log_msg("Wating for game to end", INFO_FLAG);
    sleep(13);

    // exchange scope
    recv_player_score(sock1, &pl1);
    recv_player_score(sock2, &pl2);
    if ( pl1.score > pl2.score ) {
        send_game_result(sock1, WINNER);
        send_game_result(sock2, LOOSER);
    } else {
        send_game_result(sock1, LOOSER);
        send_game_result(sock2, WINNER);
    }

    return NULL;
}

void recv_player_info(int socket, struct player *player) {
     struct player pl;

    if ( recv(socket, (void*) &pl, sizeof(struct player), 0) < 0)
        error_continue("failed to recv player information");
    else
        strcpy(player->username, pl.username);
        player->score = pl.score;
}

void send_player_info(int socket, struct player *player) {
    struct player pl;
    strcpy(pl.username, player->username);
    pl.score = player->score;

    log_msg("sending player info ...", INFO_FLAG);
    if ( send(socket, (void*) &pl, sizeof(struct player), 0 ) < 0 )
        error_exit("send player info");
}

void send_wordlist(int socket, char **wordlist, size_t wordlist_size) {
    char buffer[64];
   
    uint32_t size = htonl(wordlist_size);

    // send wordlist size first
    if ( send(socket, (void*) &size, sizeof(uint32_t), 0) < 0 ) {
        error_continue("Send wordlist size to player");
        return;
    }

    // send wordlist content
    for (int i = 0; i < wordlist_size; i++) {
        strcpy(buffer, wordlist[i]);
        if ( send(socket, (void*) buffer, sizeof(buffer), 0) < 0) {
            error_continue("send wordlist content");
            return;
        }
    }

    log_msg("wordlist content is sent succesfully", SUCCESS_FLAG);
}

char **recv_wordlist(int socket, size_t *wordlist_size) {
    char buffer[64];
    uint32_t size;
    int counter = 0;


    log_msg("recieving wordlist ...", INFO_FLAG);

    // recv wordlist size first
    if ( recv(socket, (void*) &size, sizeof(uint32_t), 0) < 0 )
        error_exit("Failed to recv wordlist size");
    else
        *wordlist_size = (size_t) ntohl(size);

    // allocate memory
    log_msg("allocating memory for wordlist ...", INFO_FLAG);
    char **wordlist = malloc(sizeof(char*) * (*wordlist_size));
    for (int i = 0; i < *wordlist_size; i++)
        *(wordlist + i) = malloc(sizeof(char) * 5);

    // recv wordlist content
    log_msg("recieving wordlist content ...", INFO_FLAG);
    for (int i = 0; i < *wordlist_size; i++) {
        if ( recv(socket, (void*) buffer, sizeof(buffer), 0) < 0 )
            error_exit("recv wordlist content");
        strcpy(wordlist[counter++], buffer);
    }

    log_msg("wordlist recieved successfully", SUCCESS_FLAG);
    return wordlist;
}


void send_matrix(int socket, char **matrix) {
    char buffer[64];
    int counter = 0; 
    // concatenate the matrix content in one string
    for (int i = 0; i < 4; i++) {
        for (int j = 0; j < 4; j++ )
            buffer[counter++] = matrix[i][j];
    }

    // send matrix sting
    if ( send(socket, (void*) buffer, sizeof(buffer), 0) < 0 )
        error_continue("send matrix content");

}

char **recv_matrix(int socket) {
    char buffer[64];
    int counter = 0;

    log_msg("recieving matrix ....",INFO_FLAG);

    // allocate memroy
    char **matrix = malloc(sizeof(char*) * 4);
    for (int i = 0; i < 4; i++)
        *(matrix + i ) = malloc(sizeof(char)*4);

    // recv matrix string
    if ( recv(socket, (void*) buffer, sizeof(buffer), 0) < 0 )
        error_exit("recv matrix content");

    // construct matrix object
    for (int i = 0; i < 4; i++) {
        for (int j = 0; j < 4; j++)
            matrix[i][j] = buffer[counter++];
    }
    return matrix;
}



int recv_notification(int socket) {
    uint32_t notification;
    int result = -1;

    if ( recv(socket, (void*) &notification, sizeof(uint32_t), 0) < 0 )
        error_continue("recv notification");
    else
        result = ntohl(notification);

    log_msg("recieved notification successfully", SUCCESS_FLAG);
    return result;
}

void send_notification(int socket, int notif) {
    uint32_t notification = htonl(notif);

    log_msg("sending notification ...", INFO_FLAG);

    if ( send(socket, (void*) &notification, sizeof(uint32_t), 0) < 0 )
        error_continue("send notification");
}

int recv_player_score(int socket, struct player *player) {
    uint32_t buffer;

    if ( recv(socket, (void*) &buffer, sizeof(uint32_t), 0) < 0 )
        error_continue("to recv player score");
    else
        player->score = (size_t) ntohl(buffer);
}

void send_player_score(int socket, struct player *player) {
    uint32_t buffer = htonl(player->score);

    if ( send(socket, (void*) &buffer, sizeof(uint32_t), 0) < 0 )
        error_exit("send player score to server");
}
    
void send_game_result(int socket, int flag) {
    uint32_t buffer = htonl(flag);

    if ( send(socket, (void*) &buffer, sizeof(uint32_t), 0) < 0 )
        error_continue("send game result");
}

int recv_game_result(int socket) {
    uint32_t buffer;

    if ( recv(socket, (void*) &buffer, sizeof(uint32_t), 0 ) < 0 )
        error_exit("failed to recv game result");

    return (int) ntohl(buffer);
}


void signal_handler(int signal_id) {
    fprintf(stdout, "[*] Signal is been handled\n");
}

