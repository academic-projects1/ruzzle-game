#ifndef GAME_H 
#define GAME_H

#include <stdio.h>
#include <stdlib.h>
#include <errno.h>
#include <string.h>
#include <ctype.h>

#include "util.h"

#define H_L_R 1
#define H_R_L 2
#define V_T_B 3
#define V_B_T 4
#define D_TL_BR 5
#define D_BR_TL 6
#define D_BL_TR 7
#define D_TR_BL 8


struct player {
    char username[32];
    size_t score;
};

struct game_data {
    struct player *player;
    int socket;

    // matrix of characters
    char **matrix;
    int matrix_size;

    // valid wordlist of words
    char **wordlist;
    int wordlist_size;

    // constructed word from user input
    char word[5];
    int word_size;
};


/******************* PLAYER **************************/

/* create *player struct from *username and score */
struct player *create_player(const char *username, size_t score);

/* addd score sc to player *pl */
void add_score(struct player *pl, size_t sc);

/* display *player username and score to terminal */
void display_player(struct player *player);



/******************* GAME DATA ********************************/

/* read n line of words from *filename and return **wordlist */
char **read_wordlist (const char *filename, int n);

/* generate **matrix from **wordlist */
char **generate_matrix (char **wordlist, size_t wordlist_size);

/* chefk if **wordlist contains *word */
int is_valid_word(char **wordlist, int wordlist_size, char* word);

/* initialize *game_data structure and assign vars */
void init_game_data(struct game_data *game_data,
                    char **matrix,
                    struct player *player,
                    char **wordlist,
                    int wordlist_size
                    );



/******************* GAME LOGIC *********************************/

/* check if it possible the place *word in **matrix (check all orientations) */
int place_word(char **matrix, char *word, size_t word_length);

/* check if placing *word in **matrix, is going to result an OVERFLOW at (x,y) with the orientation var */
int is_path_clear(char **matrix, size_t matrix_size, char *word, size_t x, size_t y, size_t orientation);

/* place in **matrix the *word at the position (x,y) with the provided orientation */
void put_word(char **matrix, char *word, size_t x, size_t y, size_t orientation);


#endif
