#include "util.h"

void log_msg(const char *msg, int flag) {
    switch ( flag ) {
        case SUCCESS_FLAG:
            fprintf(stdout, "[+] %s\n", msg);
            break;
        case INFO_FLAG:
            fprintf(stdout, "[*] %s\n", msg);
            break;
        case ERROR_FLAG:
            fprintf(stdout, "[-] Failed to %s\n", msg);
            break;
        default:
            puts("");
    }
}

void error_exit(const char *msg) {
   log_msg(msg, ERROR_FLAG);
   fprintf(stdout, "[?] Error: %s\n", strerror(errno));
   exit(EXIT_FAILURE);
}

void error_continue(const char *msg) {
   log_msg(msg, ERROR_FLAG);
   fprintf(stdout, "[?] Error: %s\n", strerror(errno));
}

void display_matrix(const char **matrix, size_t size) {
    fprintf(stdout, "==== matrix ====\n");
    for (int i = 0; i < size; i++ ) {
        for (int j = 0; j < size; j++)
            fprintf(stdout, "%c ", matrix[i][j]);
        fprintf(stdout, "\n");
    }

    fprintf(stdout, "=================\n");
}

void display_wordlist(char **wordlist, int size) {
    if ( wordlist == NULL )
        return;
    for (int i = 0; i < size; i++)
        fprintf(stdout, "[?] word: %s\n", *(wordlist+i));
}

int get_random_number(int min, int max) {
    return rand() % (max + 1 - min ) + min;
}

char *get_random_word(char **wordlist, size_t wordlist_size) {
    int i = get_random_number(0, wordlist_size);
    return *(wordlist + i);
}


char get_random_character() {
    return (char) get_random_number(97, 122);
}
struct stack *create_stack(const char *word) {
    struct stack *st = malloc(sizeof(struct stack));
    st->word = malloc(sizeof(char) * ( strlen(word) + 1));
    st->next = NULL;

    strcpy(st->word, word);
    return st;
}

void push(struct stack **st, const char *word) {
    if ( word == NULL )
        return;
    if ( *st == NULL ) {
        *st = create_stack(word); 
    } else {
        struct stack *new_stack = create_stack(word);
        new_stack->next = *st;
        *st = new_stack;
    }
}

    
char *pop(struct stack **st) {
    char *word = (*st)->word;
    *st = (*st)->next;
    return word;
}

int is_empty(struct stack *st) {
    return (st == NULL)? 1 : 0;
}


void traverse(struct stack *st) {
    if ( is_empty(st) ) {
        fprintf(stdout, "Stack is empty\n");
    } else {
        struct stack *pstack = st;
        while ( ! is_empty(pstack) ) {
            fprintf(stdout, "Stack: %s\n", pstack->word);
            pstack = pstack->next;
        }
    }
}

  
