CC := gcc
CFLAGS := -rdynamic `pkg-config --cflags --libs gtk+-3.0`

OBJS :=game.o util.o network.o gui.o

all: server client 

client: $(OBJS) 
	$(CC) $(CFLAGS) -o client client.c $^

server: $(OBJS) 
	$(CC) $(CFLAGS) -o server server.c $^

%.o: %.c
	$(CC) $(CFLAGS) $< -o $@ -c

clean:
	rm -rf *.o server client
