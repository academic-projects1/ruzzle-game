#ifndef NETWORK_H
#define NETWORK_H

#include <stdio.h>
#include <stdlib.h>
#include <sys/types.h>
#include <netinet/in.h>
#include <unistd.h>
#include <fcntl.h>
#include <errno.h>
#include <sys/time.h>
#include <net/if.h>
#include <string.h>
#include <sys/socket.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <pthread.h>
#include <signal.h>

#include "util.h"
#include "game.h"

#define exit exit(EXIT_FAILURE);
#define START_GAME 100
#define END_GAME -101

pthread_mutex_t lock;

struct request_data {
    // wordist of valid words 
    int wordlist_size;
    char **wordlist;

    // matrix of the game
    int matrix_size;
    char **matrix;
    
    // socket of two players
    int sock1;
    int sock2;

    // mutex lock and condition for synchronization between players
    pthread_mutex_t lock;
    pthread_cond_t cond;

};

/* create client or server socket */
int create_client_socket(const char *server, int port);
int create_server_socket();

/* handle requests for two players (callback for thread) */
void *handle_request(void *request_data);
void signal_handler(int signal_id);

/* recieve and send player struct */
void recv_player_info(int socket, struct player *player);
void send_player_info(int socket, struct player *player);

/* recv and send wordlist of valid words */
void send_wordlist(int socket, char **wordlist, size_t wordlist_size);
char **recv_wordlist(int socket, size_t *wordlist_size);

/* recv and send matrix of the games as single string */
void send_matrix(int socket, char **matrix);
char **recv_matrix(int socket);

/* recv and send notification to start or end the games */
int recv_notification(int socket);
void send_notification(int socket, int notif);

/* recv and send player score */
int recv_player_score(int socket, struct player *player);
void send_player_score(int socket, struct player *player);

/* recv and send game result: winner or looser */
void send_game_result(int socket, int flag);
int recv_game_result(int socket);

#endif
