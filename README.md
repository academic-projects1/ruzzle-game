# Ruzzle Game

Ruzzle is a board game developed by a swedish company *MAG Interactive*, is highly social and based on online matches between two oppenents.

Each game requires two random players, and limited by time, where each one needs to find the maximum number of valid words from a given grid of letter.
Each word has at minimum three letters, and four letter at maximum.
The game time limite is two minutes.

## Why create this game

The main purpose of making this game in C (beside being an academic project), is to implement low level concepts of networking (server/client synchronization, sockets, signal, working with GUI ... )

## Requirement & tools

The following are rquired for the compilation:

* GTK
* gcc

```
# Install gtk3 on fedora
> sudo dnf install gtk3-devel gstreamer-devel clutter-devel webkit2gtk3-devel libgda-devel gobject-introspection-devel
```

The follwing are used for development:

* gtk3 ( GUI )
* glade ( Designer )
* vim ( Editor of choise :smiely: )

```
# install gtk3 docs packages
> sudo dnf install devhelp gtk3-devel-docs gstreamer-devel-docs clutter-doc
> sudo dnf install snapd
> sudo ln -s /var/lib/snapd/snap /snap
> sudo snap install glade
```

Links: [GTK3 Installation](https://developer.fedoraproject.org/tech/languages/c/gtk.html), [Glade](https://snapcraft.io/install/glade/fedora)


## Usage:

```
# clone the repo
> git clone https://github.com/ablil/ruzzle-game
> cd ruzzle-game

# Compile
> make ruzzle
> ./ruzzle
```


## Todo
